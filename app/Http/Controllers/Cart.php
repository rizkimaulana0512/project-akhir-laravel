<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Keranjang;
use App\User;
use App\Profile;
use App\Transaksi;
use PDF;
use Alert;
use App\Struk;

class Cart extends Controller
{
    public function index(){
        // if(\Auth::user()){
        //     $data = \Auth::user()->cart;
        //     $this->validate($data);
        //     foreach($data as $key=>$value){
        //         \Cart::add(array(
        //             'id' => $value->id,
        //             'name' => $value->nama,
        //             'price' => $value->harga,
        //             'quantity' => $value->kuantitas,
        //         ));
        //     }
        // }
            $cart = \Cart::getContent();
        return view('cart',compact('cart'));
    }

    public function add(Request $request){
        $validateData = $request->validate(
            [
                'nama' => 'required',
                'harga' => 'required',
                // 'bio' => 'required'
            ]);
        \Cart::add(array(
            'id' => $request->id,
            'name' => $request->nama,
            'price' => $request->harga,
            'quantity' => 1,
        ));
        $data = \Cart::get($request->id);
        if(\Auth::user() != Null){
            Keranjang::updateOrInsert(['produk_id' => $request->id,'user_id'=>\Auth::user()->id],['kuantitas'=>$data->quantity,'total'=>$data->price * $data->quantity]);
        }
        return \Redirect::back()->with('message','Operation Successful !');
    }
    public function remove($id){
        \Cart::remove($id);
        if(\Auth::user() != Null){
            Keranjang::where(['produk_id' =>$id,'user_id'=>\Auth::user()->id])->delete();
        }
        return redirect('/cart');
    }

    public function checkout(){
        $cart = \Cart::getContent();
        if(\Auth::user()){
            foreach($cart as $key=>$value){
                Keranjang::updateOrInsert(['produk_id' => $value->id,'user_id'=>\Auth::user()->id],['kuantitas'=>$value->quantity,'total'=>$value->price * $value->quantity]);
            }
            $user =\Auth::user()->profile;
            $cart =\Auth::user()->cart;
            
            return view('checkout',compact('cart','user'));
        }else
            return redirect('/login');
    }

    public function ordered(Request $request){
        $validateData = $request->validate([
            'nama' => 'required',
            'email'=> 'required',
            'alamat'=> 'required',
            'no_hp'=> 'required',
        ]);
        $data = array(
            'nama' => $request->nama,
            'email'=> $request->email,
            'alamat'=> $request->alamat,
            'no_hp'=> $request->no_hp,
        );
        Profile::updateOrInsert(['user_id'=>\Auth::user()->id],$data);
        $cart = \Auth::user()->cart;
        $kuantitas = $total = 0;
        foreach($cart as $key=>$value){
            $kuantitas += $value->pivot->kuantitas; 
            $total += $value->pivot->total; 
        }
        $trans = array(
            'tgl_transaksi' => date('Y-m-d'),
            'kuantitas' => $kuantitas,
            'total' => $total,
            'status' => 'not paid',
            'struk' => date('Y-m-d_H``i`s').'_'.\Auth::user()->id,
            'user_id' =>\Auth::user()->id,
        );
        $trans_id = Transaksi::insertGetId($trans);
        foreach($cart as $key=>$value){
            $struk= array(
                'transaksi_id' =>$trans_id,
                'produk_id' => $value->id,
                'kuantitas' => $value->pivot->kuantitas,
                'total' => $value->pivot->total,
            );
            Struk::insert($struk);
        }
        $path = 'pdf/'.date('Y-m-d-H:i:s').'_'.\Auth::user()->id.'.pdf';
        // echo $path;
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadView('temp.struk', compact('cart','kuantitas','total'))->save('pdf/'.date('Y-m-d_H``i`s').'_'.\Auth::user()->id.'.pdf');
        Keranjang::where('user_id',\Auth::user()->id)->delete();
        // return PDF::loadFile(public_path().'/')->save('pdf/'.date('Y-m-d H:i:s').'_'.\Auth::user()->id.'.pdf')->stream('download.pdf');
        Alert::success('Success','Order Has Been Made');
        return redirect('/profile');
    }
}
