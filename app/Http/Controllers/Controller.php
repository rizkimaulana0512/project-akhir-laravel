<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use DB;
use PDF;
use App\Produk;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function home(){
        $produk = Produk::get();
        
        return view('index',compact('produk'));
    }

    public function file($jenis,$file){
        if($jenis == 'struk'){
            $pdf = \App::make('dompdf.wrapper');
            return $pdf->loadFile(public_path().'/pdf/'.$file.'.pdf')->stream('download.pdf');
        }
    }
}
