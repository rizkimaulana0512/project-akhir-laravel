<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Kategori;

class KategoriController extends Controller
{
    public function create ()
    {
        return view ('kategori.create');
    }

    public function store(Request $request)
    {
        $request->validate(
            [
                'nama' => 'required',
            ],

            [
                'nama.required' => 'Masukkan nama kategori',
            ]
            );

        Kategori::insert(
            [
                'nama'=>$request['nama']
            ]
            );

        return redirect('/kategori');
    }

    public function index()
    {
        $kategori = Kategori::get();

        return view('kategori.index', compact('kategori'));
    }
}
