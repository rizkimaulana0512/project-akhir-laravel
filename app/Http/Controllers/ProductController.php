<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Produk;
use App\Kategori;
class ProductController extends Controller
{
    public function index ()
    {
        $produk = Produk::get();
        return view ('produk.index',compact('produk'));
    }
    public function create(){
        $kategori = Kategori::get();
        return view ('produk.create',compact('kategori'));
    }
    public function store(Request $request){
        $data=array(
            'nama' => $request->name,
            'harga' => $request->harga,
            'deskripsi' => $request->deskripsi,
            'kategori_id' => $request->kategori,
        );
        Produk::insert($data);
        return redirect('/produk');
    }
}
