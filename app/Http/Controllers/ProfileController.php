<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Profile;
use Alert;
use App\Transaksi;
class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(){
        $data = \Auth::user()->profile;
        $trans = \Auth::user()->transaksi;
        return view('profile.profile',compact('data','trans'));
    }
    public function bukti(Request $request){
        if($request->hasFile('bukti')){
            $file = $request->bukti->store('bukti');
            Transaksi::where('id',$request->id)->update(['bukti'=>$file]);
            Alert::success('Success','Upload Succes, Please Wait for Confirmation');
        }
        else{
            Alert::error('File Not Found','Please Upload Valid File');
        }
        return redirect('/profile');
    }

    public function edit(){
        $user = \Auth::user()->profile;
        return view('profile.edit',compact('user'));
    }
    public function update(Request $request){
        $data = array(
            'nama' => $request->nama,
            'email'=> $request->email,
            'alamat'=> $request->alamat,
            'no_hp'=> $request->no_hp,
        );
        Profile::where('user_id',\Auth::user()->id)->update($data);
        return redirect('/profile');
    }
}
