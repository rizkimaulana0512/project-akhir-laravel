<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;
class Keranjang extends Pivot
{
    protected $table ='produk_user';
    public function user(){
        return $this->belongsTo('App\User');
    }
    public function produk(){
        return $this->belongsTo('App\Produk');
    }
}
