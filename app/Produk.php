<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Produk extends Model
{
    public function kategori(){
        return $this->belongsTo('App\Kategori');
    }

    public function cart(){
        // return $this->belongsToMany('App\User')->using('App\Keranjang');
        return $this->belongsToMany('App\User')->using('App\Keranjang');
    }

    public function struk(){
        return $this->belongsToMany('App\Transaksi');
    }
}
