<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaksi extends Model
{
    public function user(){
        return $this->belongsTo('App\User');
    }

    public function struk(){
        return $this->belongsToMany('App\Produk');
    }
}
