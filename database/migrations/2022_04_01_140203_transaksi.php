<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Transaksi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaksis',function(Blueprint $table){
            $table->bigIncrements('id');
            $table->dateTime('tgl_transaksi');
            $table->integer('kuantitas');
            $table->integer('total');
            $table->enum('status',['paid','not paid']);
            $table->text('bukti');
            $table->text('struk');
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
