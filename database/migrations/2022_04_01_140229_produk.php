<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Produk extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('produks',function (Blueprint $table){
            $table->bigIncrements('id');
            $table->string('nama');
            $table->text('deskripsi');
            $table->integer('harga');
            $table->integer('stok');
            $table->text('gambar');
            $table->unsignedBigInteger('kategori_id');
            $table->timestamps();
        });
        Schema::table('struks',function(Blueprint $table){
            $table->foreign('produk_id')->references('id')->on('produks');
        });
        Schema::table('produk_user',function(Blueprint $table){
            $table->foreign('produk_id')->references('id')->on('produks');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
