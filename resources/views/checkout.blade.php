@extends('temp/template')
@section('content')
<div class="breadcrumb-section breadcrumb-bg">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 offset-lg-2 text-center">
					<div class="breadcrumb-text">
						<p>Fresh and Organic</p>
						<h1>Check Out Product</h1>
					</div>
				</div>
			</div>
		</div>
	</div>
<div class="checkout-section mt-150 mb-150">
		<div class="container">
			<div class="row">
				<div class="col-lg-8">
					<div class="checkout-accordion-wrap">
						<div class="accordion" id="accordionExample">
						  <div class="card single-accordion">
						    <div class="card-header" id="headingOne">
						      <h5 class="mb-0">
						        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
						          Billing Address
						        </button>
						      </h5>
						    </div>

						    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
						      <div class="card-body">
						        <div class="billing-address-form">
						        	<form action="/ordered" method="post">
                                        @csrf
						        		<p><input type="text" placeholder="Name" name="nama" value="{{$user!= NUll ? $user->nama : ''}}"></p>
						        		<p><input type="email" placeholder="Email" name="email" value="{{$user!= NUll ? $user->email: ''}}"></p>
						        		<p><input type="text" placeholder="Address" name="alamat" value="{{$user!= NUll ? $user->alamat :''}}"></p>
						        		<p><input type="tel" placeholder="Phone" name="no_hp" value="{{$user!= NUll ? $user->no_hp :''}}"></p>
                                        <button type="submit" class="boxed-btn">
                                            <a class="boxed-btn">Place Order</a>
                                        </button>
						        	</form>
						        </div>
						      </div>
						    </div>
						  </div>
						</div>
					</div>
				</div>

				<div class="col-lg-4">
					<div class="order-details-wrap">
						<table class="order-details">
							<thead>
								<tr>
									<th>Your order Details</th>
									<th>Price</th>
									<th>Quantity</th>
									<th>Total</th>
								</tr>
							</thead>
							<tbody class="order-details-body">
								@foreach($cart as $key=>$value)
                                    <tr>
                                        <td>{{$value->nama}}</td>
                                        <td>{{$value->harga}}</td>
                                        <td>{{$value->pivot->kuantitas}}</td>
                                        <td>{{$value->pivot->total}}</td>
                                    </tr>
                                @endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection