@extends('temp/template')

@section('content')
<div class="breadcrumb-section breadcrumb-bg">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 offset-lg-2 text-center">
					<div class="breadcrumb-text">
						<h1>LIST KATEGORI</h1>
					</div>
				</div>
			</div>
		</div>
	</div>
<div class="list-section pt-80 pb-80">
<div class="container">
    <table class="table">
        <thead>
            <tr>
            <th scope="col">#</th>
            <th scope="col">Nama Produk</th>
            <th scope="col">Deskripsi Produk</th>
            <th scope="col">Harga Produk</th>
            <th scope="col">Action</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($produk as $key => $item)
                <tr>
                    <td>{{$key + 1}}</td>
                    <td>{{$item->nama}}</td>
                    <td>{{$item->deskripsi}}</td>
                    <td>{{$item->harga}}</td>
                    <td>
                        <a href="" class="btn btn info btn-sm">Detail</a>
                    </td>
                </tr>

            @empty

            @endforelse
        </tbody>
        
<a href="/produk/create" class="btn btn-primary my-3">Tambah Kategori</a>
    </table>
</div>

</div>
@endsection
