@extends('temp/template')

@section('content')
<div class="breadcrumb-section breadcrumb-bg">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 offset-lg-2 text-center">
					<div class="breadcrumb-text">
						<h1>CREATE PRODUK</h1>
					</div>
				</div>
			</div>
		</div>
	</div>
<div class="list-section pt-80 pb-80">
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Edit Profile') }}</div>

                <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
						      <div class="card-body">
						        <div class="billing-address-form">
						        	<form action="/profile/edit" method="post">
                                        @csrf
						        		<p><input type="text" placeholder="Name" name="nama" value="{{$user!= NUll ? $user->nama : ''}}"></p>
						        		<p><input type="email" placeholder="Email" name="email" value="{{$user!= NUll ? $user->email: ''}}"></p>
						        		<p><input type="text" placeholder="Address" name="alamat" value="{{$user!= NUll ? $user->alamat :''}}"></p>
						        		<p><input type="tel" placeholder="Phone" name="no_hp" value="{{$user!= NUll ? $user->no_hp :''}}"></p>
                                        <button type="submit" class="boxed-btn">
                                            <a class="boxed-btn">Place Order</a>
                                        </button>
						        	</form>
						        </div>
						      </div>
						    </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection
