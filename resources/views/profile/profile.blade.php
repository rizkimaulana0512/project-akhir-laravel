@extends('temp/template')
@section('content')
    <div class="breadcrumb-section breadcrumb-bg">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 offset-lg-2 text-center">
					<div class="breadcrumb-text">
						<p>Fresh and Organic</p>
						<h1>Profile</h1>
					</div>
				</div>
			</div>
		</div>
	</div>
    <div class="checkout-section mt-150 mb-150">
		<div class="container" style=" margin-left:0px;">
			<div class="row" style="width:2000px;">
				<div class="col-lg-7">
					<div class="checkout-accordion-wrap">
						<div class="accordion" id="accordionExample">
						  <div class="card single-accordion">
						    <div class="card-header" id="headingOne">
						      <h5 class="mb-0">
						        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
						          Transaksi Sebelumnya
						        </button>
						      </h5>
						    </div>

						    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
						      <div class="card-body">
						        <div class="">
						        	<table style="width:100%; border:solid 1px;">
                                        <thead>
                                            <tr class="table-total-row">
                                                <th>#</th>
                                                <th>Tanggal Transaksi</th>
                                                <th>Jumlah Barang</th>
                                                <th>Total</th>
                                                <th>Status</th>
                                                <th>Struk</th>
                                                <th>Bukti</th>
                                            </tr>
                                        </thead>
                                        <tbody style="text-align:center">
                                            @foreach($trans as $key=>$value)
                                                <tr>
                                                    <td>{{$key + 1 }}</td>
                                                    <td>{{$value->tgl_transaksi }}</td>
                                                    <td>{{$value->kuantitas }}</td>
                                                    <td>{{$value->total }}</td>
                                                    <td>{{$value->status }}</td>
                                                    <td><a href="/file/struk/{{$value->struk}}">
                                                        {{$value->struk }}</td>
                                                    </a>
                                                    <td>
                                                        @if($value->bukti == Null)
                                                            <form action="/profile/bukti" method="post" enctype="multipart/form-data">
                                                                @csrf
                                                                <input type="text" hidden value="{{$value->id}}" name="id">
                                                                <input type="file" name="bukti">
                                                                <button type="submit">Upload</button>
                                                            </form>    
                                                            
                                                        @else
                                                            <a href="/file/bukti/{{$value->bukti}}">
                                                                {{$value->bukti }}
                                                            </a>
                                                        @endif
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
						        </div>
						      </div>
						    </div>
						  </div>
						</div>
					</div>
				</div>

				<div class="col-lg-4">
					<div class="order-details-wrap">
						<table class="order-details">
							<thead>
								<tr>
									<th>Nama:</th>
									<th>{{\Auth::user()->profile->nama}}</th>
								</tr>
								<tr>
									<th>Alamat:</th>
									<th>{{\Auth::user()->profile->alamat}}</th>
								</tr>
								<tr>
									<th>Email:</th>
									<th>{{\Auth::user()->profile->email}}</th>
								</tr>
								<tr>
									<th>No Hp:</th>
									<th>{{\Auth::user()->profile->no_hp}}</th>
								</tr>
							</thead>
							<tbody class="order-details-body">
                                <a href="/profile/edit" class="btn btn-primary my-3">Edit Profile</a>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>

@endsection