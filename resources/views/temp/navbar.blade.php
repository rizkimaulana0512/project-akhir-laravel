<div class="top-header-area" id="sticker">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-sm-12 text-center">
					<div class="main-menu-wrap">
						<!-- logo -->
						<div class="site-logo">
							<a href="index.html">
								<img src="{{asset('assets/img/logo.png')}}')}}" alt="">
							</a>
						</div>
						<!-- logo -->

						<!-- menu start -->
						<nav class="main-menu">
							<ul>
								<li class="current-list-item"><a href="/">Home</a>
								</li>
								@guest
								@else
									@if(\Auth::user()->role=='Admin')
										<li><a href="news.html">Kategori</a>
											<ul class="sub-menu">
												<li><a href="/kategori">Kategori</a></li>
												<li><a href="/kategori/create">Create Kategori</a></li>
											</ul>
										</li>
										<li><a href="">Produk</a>
											<ul class="sub-menu">
												<li><a href="/produk">Produk</a></li>
												<li><a href="/produk/create">Create Produk</a></li>
											</ul>
										</li>
										<li><a href="">Transaksi</a>
											<ul class="sub-menu">
												<li><a href="/transaksi">Transaksi</a></li>
												<li><a href="/transaksi/create">Create Transaksi</a></li>
											</ul>
										</li>
										<li><a href="">Profile</a>
											<ul class="sub-menu">
												<li><a href="/profile">Profile</a></li>
												<li><a href="/profile/create">Create Profile</a></li>
											</ul>
										</li>
										<li><a href="">User</a>
											<ul class="sub-menu">
												<li><a href="/user">User</a></li>
												<li><a href="/user/create">Create User</a></li>
											</ul>
										</li>
										<li><a href="">Struk</a>
											<ul class="sub-menu">
												<li><a href="/struk">Struk</a></li>
												<li><a href="/struk/create">Create Struk</a></li>
											</ul>
										</li>
									@endif
								@endguest
								<li>
									<div class="header-icons">
										<a class="shopping-cart" href="/cart"><i class="fas fa-shopping-cart"></i></a>
										<a class="mobile-hide search-bar-icon" href="#"><i class="fas fa-search"></i></a>
										@guest
												<a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
											@if (Route::has('register'))
													<a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
											@endif
										@else
											
												<a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
													{{ Auth::user()->name }} <span class="caret"></span>
												</a>
												<ul class="sub-menu">
													<li>
														<a href="/profile">Profile</a>
													</li>
													<li>
															<a class="dropdown-item" href="{{ route('logout') }}"
															onclick="event.preventDefault();
																	document.getElementById('logout-form').submit();">
															{{ __('Logout') }}
															</a>
															
															<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
																@csrf
															</form>
													</li>
												</ul>
										@endguest
									</div>
								</li>
							</ul>
						</nav>
						<a class="mobile-show search-bar-icon" href="#"><i class="fas fa-search"></i></a>
						<div class="mobile-menu"></div>
						<!-- menu end -->
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- end header -->
	
	<!-- search area -->
	<div class="search-area">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<span class="close-btn"><i class="fas fa-window-close"></i></span>
					<div class="search-bar">
						<div class="search-bar-tablecell">
							<h3>Search For:</h3>
							<input type="text" placeholder="Keywords">
							<button type="submit">Search <i class="fas fa-search"></i></button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>