<div>
    <div style="text-align:center">
        <h1>Struk Transaksi</h1>
        <p>{{\Auth::user()->name}} - {{date('Y-m-d H-i-s')}}</p>
        <p>{{\Auth::user()->profile->email}} - {{\Auth::user()->profile->alamat}}</p>
    </div>
    <hr>
    <div>
        <table style="width:100%">
            <thead>
                <th>#</th>
                <th>Nama Barang</th>
                <th>Harga</th>
                <th>Jumlah</th>
                <th>Total</th>
            </thead>
            <tbody style="text-align:center">
                @foreach($cart as $key=>$value)
                <tr >
                    <td>{{$key +1}}</td>
                    <td>{{$value->nama}}</td>
                    <td>{{$value->harga}}</td>
                    <td>{{$value->pivot->kuantitas}}</td>
                    <td>{{$value->pivot->total}}</td>
                </tr>
                @endforeach
                <tr>
                    <td><hr></td>
                    <td><hr></td>
                    <td><hr></td>
                    <td><hr></td>
                    <td><hr></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td>Total</td>
                    <td>{{$kuantitas}}</td>
                    <td>{{$total}}</td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
    