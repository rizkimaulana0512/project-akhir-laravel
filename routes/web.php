<?php
use App\Http\controllers\Cart; 
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/','Controller@home');
//Cart
Route::get('/cart',[Cart::class,'index']);
Route::post('/cart/add',[Cart::class,'add']);
Route::post('/ordered',[Cart::class,'ordered']);
Route::get('/cart/remove/{id}',[Cart::class,'remove']);
Route::get('/checkout',[Cart::class,'checkout']);


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//membuat create kategori
Route::get('/kategori/create', 'KategoriController@create');
Route::post('/kategori', 'KategoriController@store');
Route::get('/kategori', 'KategoriController@index');

//CRUD Profile
Route::get('/profile','ProfileController@index');
Route::get('/profile/edit','ProfileController@edit');
Route::post('/profile/edit','ProfileController@update');
Route::post('/profile/bukti','ProfileController@bukti');

//File
Route::get('/file/{jenis}/{file}','Controller@file');
//CRUD Produk
Route::get('/produk/create', 'ProductController@create');
Route::get('/produk', 'ProductController@index');
Route::delete('/produk/{id}', 'ProductController@index');
Route::post('/produk', 'ProductController@store');

//CRUD User
Route::get('/user','UserController@index');
Route::get('/user/create','UserController@create');